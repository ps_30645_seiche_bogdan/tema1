package entities

import database.commons.UserType

/**
 *
 */
data class UserDto(
    val username: String,
    val name: String,
    val password: String,
    val type: UserType
)