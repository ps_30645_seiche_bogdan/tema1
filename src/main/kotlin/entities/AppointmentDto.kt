package entities

import database.commons.ServiceType
import java.time.Instant
import java.util.*

/**
 *
 */
data class AppointmentDto(
    val id: UUID,
    val clientName: String,
    val time: Instant,
    val clientPhone: String,
    val service: ServiceType,
    val creator: String
)