package utils

import repos.AppointmentsRepository
import repos.UsersRepository
import repos.`interface`.IAppointmentsRepository
import repos.`interface`.IUsersRepository
import org.koin.dsl.module

/**
 *
 */
val appDependencies = module {

    // Singleton (returns always the same unique instance of the object)
    single<IUsersRepository> { UsersRepository() }
    single<IAppointmentsRepository> { AppointmentsRepository() }

    // Transient (returns always the a new instance of the object)
    // factory { FakeInMemoryUsersRepository() }

}