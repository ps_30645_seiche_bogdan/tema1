package service

import database.commons.UserType
import repos.`interface`.IUsersRepository
import entities.UserDto
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import utils.getMd5FromString

/**
 *
 */
@KoinApiExtension
object UserService : KoinComponent {
    private val usersRepository: IUsersRepository by inject()

    fun login(username: String, password: String): UserType? {
        val users = usersRepository.getByUserName(username)
        users.forEach {
            if (it.password == getMd5FromString(username)) {
                return it.type
            }
        }
        return null
    }

    fun addUser(userDto: UserDto) = usersRepository.insert(userDto)

    fun getAllUsers() = usersRepository.getAll()

    fun updateUser(userDto: UserDto) = usersRepository.updateUser(userDto)
}