package service

import repos.`interface`.IAppointmentsRepository
import repos.`interface`.IUsersRepository
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 *
 */
@KoinApiExtension
object ReportsService : KoinComponent {
    private val usersRepository: IUsersRepository by inject()
    private val appointmentsRepository: IAppointmentsRepository by inject()

}