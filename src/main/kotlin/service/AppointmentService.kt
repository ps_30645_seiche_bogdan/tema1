package service

import entities.AppointmentDto
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import repos.`interface`.IAppointmentsRepository
import java.time.Instant

/**
 *
 */
@KoinApiExtension
object AppointmentService : KoinComponent {
    private val appointmentsRepository: IAppointmentsRepository by inject()

    fun getApposForClient(clientName: String) =
        appointmentsRepository.getAll().filter { it.clientName == clientName }

    fun getAppointmentsInInterval(startTime: Instant, endTime: Instant) {
        appointmentsRepository.getAll().filter { it.time in startTime..endTime }
    }

    fun createAppointment(appointmentDto: AppointmentDto) {
        appointmentsRepository.insert(appointmentDto)
    }

    fun delete(appointmentDto: AppointmentDto) {
        appointmentsRepository.deleteById(appointmentDto.id)
    }


}