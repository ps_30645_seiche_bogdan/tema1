package repos

import repos.`interface`.IUsersRepository
import database.tables.UsersTable
import entities.UserDto
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

/**
 *
 */
class UsersRepository : IUsersRepository {
    override fun insert(userDto: UserDto): Unit = transaction {
        UsersTable.insert {
            it[username] = userDto.username
            it[name] = userDto.name
            it[password] = userDto.password
            it[type] = userDto.type
        }
    }

    override fun getAll(): List<UserDto> = transaction {
        UsersTable.selectAll().map {
            UserDto(
                username = it[UsersTable.username],
                name = it[UsersTable.name],
                password = it[UsersTable.password],
                type = it[UsersTable.type]
            )
        }
    }

    override fun getByUserName(username: String): List<UserDto> = transaction {
        UsersTable.select { UsersTable.username eq username }.map {
            UserDto(
                username = it[UsersTable.username],
                name = it[UsersTable.name],
                password = it[UsersTable.password],
                type = it[UsersTable.type]
            )
        }
    }

    override fun deleteUser(username: String): Unit = transaction {
        UsersTable.deleteWhere { UsersTable.username eq username }
    }

    override fun updateUser(userDto: UserDto): Unit = transaction {
        UsersTable.update({
            UsersTable.username eq userDto.username
        }) {
            it[username] = userDto.username
            it[name] = userDto.name
            it[password] = userDto.password
            it[type] = userDto.type
        }
    }
}