package repos

import repos.`interface`.IAppointmentsRepository
import database.tables.AppointmentsTable
import entities.AppointmentDto
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*

/**
 *
 */
class AppointmentsRepository : IAppointmentsRepository {
    override fun insert(appointmentDto: AppointmentDto): Unit = transaction {
        AppointmentsTable.insert {
            it[id] = appointmentDto.id
            it[clientName] = appointmentDto.clientName
            it[time] = appointmentDto.time
            it[clientPhone] = appointmentDto.clientPhone
            it[service] = appointmentDto.service
            it[creator] = appointmentDto.creator
        }
    }

    override fun getAll(): List<AppointmentDto> = transaction {
        AppointmentsTable.selectAll().map {
            AppointmentDto(
                id = it[AppointmentsTable.id],
                clientName = it[AppointmentsTable.clientName],
                time = it[AppointmentsTable.time],
                clientPhone = it[AppointmentsTable.clientPhone],
                service = it[AppointmentsTable.service],
                creator = it[AppointmentsTable.creator]
            )
        }
    }

    override fun getByCreator(creator: String): List<AppointmentDto> = transaction {
        AppointmentsTable.select { AppointmentsTable.creator eq creator }.map {
            AppointmentDto(
                id = it[AppointmentsTable.id],
                clientName = it[AppointmentsTable.clientName],
                time = it[AppointmentsTable.time],
                clientPhone = it[AppointmentsTable.clientPhone],
                service = it[AppointmentsTable.service],
                creator = it[AppointmentsTable.creator]
            )
        }
    }

    override fun deleteById(id: UUID): Unit = transaction {
        AppointmentsTable.deleteWhere { AppointmentsTable.id eq id }
    }

    override fun update(appointmentDto: AppointmentDto): Unit = transaction {
        AppointmentsTable.update({
            AppointmentsTable.id eq appointmentDto.id
        }) {
            it[id] = appointmentDto.id
            it[clientName] = appointmentDto.clientName
            it[time] = appointmentDto.time
            it[clientPhone] = appointmentDto.clientPhone
            it[service] = appointmentDto.service
            it[creator] = appointmentDto.creator
        }
    }
}