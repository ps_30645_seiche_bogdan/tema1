package repos.`interface`

import entities.AppointmentDto
import java.util.*

/**
 *
 */
interface IAppointmentsRepository {
    fun insert(appointmentDto: AppointmentDto)
    fun getAll(): List<AppointmentDto>
    fun getByCreator(creator: String): List<AppointmentDto>
    fun deleteById(id: UUID)
    fun update(appointmentDto: AppointmentDto)
}