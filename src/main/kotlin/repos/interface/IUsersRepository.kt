package repos.`interface`

import entities.UserDto

/**
 *
 */
interface IUsersRepository {
    fun insert(userDto: UserDto)
    fun getAll(): List<UserDto>
    fun getByUserName(username: String): List<UserDto>
    fun deleteUser(username: String)
    fun updateUser(userDto: UserDto)
}