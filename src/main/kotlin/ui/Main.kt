package ui

import database.DbSettings
import database.tables.AppointmentsTable
import database.tables.UsersTable
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.component.KoinApiExtension
import org.koin.core.context.startKoin
import tornadofx.*
import utils.appDependencies

/**
 *
 */
@KoinApiExtension
class MainUi : App(LoginView::class)

@KoinApiExtension
fun main(args: Array<String>) {
    DbSettings.initDb()
    transaction {
        SchemaUtils.create(
            UsersTable,
            AppointmentsTable
        )
    }
    startKoin {
        modules(appDependencies)
    }
    launch<MainUi>(args)
}

