package ui

import database.commons.UserType
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.Alert
import javafx.scene.control.PasswordField
import javafx.scene.control.TextField
import javafx.scene.layout.Priority
import org.koin.core.component.KoinApiExtension
import service.UserService
import tornadofx.*

/**
 *
 */
@KoinApiExtension
class LoginView : View("Beauty center") {

    private var userNameField: TextField by singleAssign()
    private var passwordField: PasswordField by singleAssign()

    override val root = vbox {
        prefWidth = 800.0
        prefHeight = 600.0
        alignment = Pos.CENTER

        style {
            padding = box(8.px)
        }
        form {
            style {
                padding = box(50.px)
            }
            fieldset("Login", labelPosition = Orientation.VERTICAL) {
                field("Username", Orientation.VERTICAL) {
                    userNameField = textfield {
                        vgrow = Priority.ALWAYS
                    }
                }

                field("Username", Orientation.VERTICAL) {
                    passwordField = passwordfield {
                        vgrow = Priority.ALWAYS
                    }
                }
                buttonbar {
                    button("Login") {
                        action { doLogin() }
                    }
                }
            }
        }
    }

    @KoinApiExtension
    private fun doLogin() {
        val userName = userNameField.text
        val password = passwordField.text
        val loggedUserType = UserService.login(userName, password);
        loggedUserType?.let {
            showMainPage(it)
        } ?: showLoginError()
    }

    private fun showMainPage(userType: UserType) {
        when (userType) {
            UserType.EMPLOYEE -> TODO()
            UserType.ADMIN -> replaceWith(MainAdminView::class)
        }
        println("Login $userType")
    }

    private fun showLoginError() {
        println("Login ERROR!!")
        alert(
            type = Alert.AlertType.ERROR,
            "Login Error",
            content = "The username or password is incorrect!",
        )
    }
}