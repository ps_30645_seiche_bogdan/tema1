package ui

import database.commons.UserType
import entities.UserDto
import javafx.collections.ObservableList
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.*
import javafx.scene.layout.Priority
import org.koin.core.component.KoinApiExtension
import service.UserService
import tornadofx.*
import utils.getMd5FromString

/**
 *
 */
@KoinApiExtension
class MainAdminView : View("Beauty center - Admin") {
    private var userNameField: TextField by singleAssign()
    private var fullNameField: TextField by singleAssign()
    private var passwordField: PasswordField by singleAssign()
    private var adminField: CheckBox by singleAssign()

    private var userTableView: TableView<UserDto> by singleAssign()

    private var userList: ObservableList<UserDto> = UserService.getAllUsers().asObservable()

    override val root = vbox {
        prefWidth = 800.0
        prefHeight = 600.0

        style {
            padding = box(8.px)
        }

        tabpane {
            tab("Users") {
                isClosable = false
                vbox {
                    alignment = Pos.CENTER
                    form {
                        style {
                            padding = box(50.px)
                        }
                        userTableView = tableview(userList) {
                            readonlyColumn("Username", UserDto::username)
                            readonlyColumn("Full Name", UserDto::name)
                            readonlyColumn("Type", UserDto::type)
                        }
                        fieldset("Create User", labelPosition = Orientation.VERTICAL) {
                            field("Username", Orientation.VERTICAL) {
                                userNameField = textfield {
                                    vgrow = Priority.ALWAYS
                                }
                            }
                            field("Password", Orientation.VERTICAL) {
                                passwordField = passwordfield {
                                    vgrow = Priority.ALWAYS
                                }
                            }
                            field("Full Name", Orientation.VERTICAL) {
                                fullNameField = textfield {
                                    vgrow = Priority.ALWAYS
                                }
                            }
                            field("Admin", Orientation.VERTICAL) {
                                adminField = checkbox {
                                    vgrow = Priority.ALWAYS
                                }
                            }


                            buttonbar {
                                button("Save user") {
                                    action { createUser() }
                                }
                            }
                        }
                    }
                }
            }
            tab("Reports") {
                isClosable = false
                vbox {
                    alignment = Pos.CENTER
                    button("Button 3")
                    button("Button 4")
                }
            }
        }

        
    }

    private fun createUser() {
        val userDto = UserDto(
            userNameField.text,
            fullNameField.text,
            password = getMd5FromString(passwordField.text),
            type = if (adminField.isSelected) {
                UserType.ADMIN
            } else {
                UserType.EMPLOYEE
            }
        )
        UserService.addUser(userDto)
        userList = UserService.getAllUsers().asObservable()
        alert(
            type = Alert.AlertType.INFORMATION,
            "User Created Successfully",
            content = "The new user ${userNameField.text} created successfully!",
        )
        userNameField.text = ""
        fullNameField.text = ""
        passwordField.text = ""
        adminField.isSelected = false
    }
}