package database.commons

/**
 *
 */
enum class UserType {
    EMPLOYEE,
    ADMIN
}

enum class ServiceType {
    NAILS,
    HAIR,
    FACE,
    MASSAGE
}