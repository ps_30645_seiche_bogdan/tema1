package database

import org.jetbrains.exposed.sql.Database


/**
 *
 */
object DbSettings {

    fun initDb() {
        Database.connect("jdbc:h2:./myH2Db", "org.h2.Driver")
    }
}
