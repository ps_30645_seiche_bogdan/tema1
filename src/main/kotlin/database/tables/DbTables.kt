package database.tables

import database.commons.ServiceType
import database.commons.UserType
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.`java-time`.timestamp
import java.time.Instant
import java.util.*

/**
 *
 */
object UsersTable : Table("users") {
    val username: Column<String> = varchar("username", 20)
    val name: Column<String> = varchar("name", 255)
    val password: Column<String> = varchar("password", 255)
    val type: Column<UserType> = enumerationByName("type", klass = UserType::class, length = 30)
    override val primaryKey = PrimaryKey(username, name = "PK_Users")
}

object AppointmentsTable : Table("users") {
    val id: Column<UUID> = uuid("uuid").autoGenerate()
    val clientName: Column<String> = varchar("clientName", 255)
    val time: Column<Instant> = timestamp("time")
    val clientPhone: Column<String> = varchar("phone", 10)
    val service: Column<ServiceType> = enumerationByName("type", klass = ServiceType::class, length = 30)
    val creator: Column<String> = varchar("creator", 20) references UsersTable.username
    override val primaryKey = PrimaryKey(id, name = "PK_Appointments")
}